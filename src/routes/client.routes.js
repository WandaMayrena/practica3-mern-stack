const express = require('express');
const router = express.Router();

const Cliente = require('../models/client');

router.get('/', async (req, res) => {
	const clientes = await Cliente.find();
	res.json(clientes);
});

router.get('/:id', async (req, res) => {
	const cliente = await Cliente.findById(req.params.id);
	res.json(cliente);
});

router.post('/', async (req, res) => {
	const { nombre, cedula, edad, genero, fecha_nac, direccion } = req.body;
	const cliente = new Cliente({nombre, cedula, edad, genero, fecha_nac, direccion });
	await cliente.save();
	res.json({status: 'Client Saved'});
}); 

router.put('/:id', async (req, res) => {
	const { nombre, cedula, edad, genero, fecha_nac, direccion } = req.body;
	const newCliente = { nombre, cedula, edad, genero, fecha_nac, direccion };
	await Cliente.findByIdAndUpdate(req.params.id, newCliente);
	res.json({status: 'Client Updated!'});
});

router.delete('/:id', async (req, res) => {
	await Cliente.findByIdAndRemove(req.params.id);
	res.json({status: 'Client Deleted'});
});

module.exports = router; 