const express = require('express');
const router = express.Router();

const PersAdm = require('../models/persAdm');

router.get('/', async (req, res) => {
	const pers = await PersAdm.find();
	res.json(pers);
});

router.get('/:id', async (req, res) => {
	const persAdm = await PersAdm.findById(req.params.id);
	res.json(persAdm);
});

router.post('/', async (req, res) => {
	const { nombre, cedula, edad, genero, fecha_nac, direccion, usuario, contraseña, email } = req.body;
	const pers = new PersAdm({nombre, cedula, edad, genero, fecha_nac, direccion, usuario, contraseña, email });
	await pers.save();
	res.json({status: 'Personal Saved'});

});

router.put('/:id', async (req, res) => {
	const { nombre, cedula, edad, genero, fecha_nac, direccion, usuario, contraseña, email } = req.body;
	const newPers = { nombre, cedula, edad, genero, fecha_nac, direccion,usuario, contraseña, email };
	await PersAdm.findByIdAndUpdate(req.params.id, newPers);
	res.json({status: 'Person Updated!'});
});

router.delete('/:id', async (req, res) => {
	await PersAdm.findByIdAndRemove(req.params.id);
	res.json({status: 'Person Deleted'});
});

module.exports = router;