const express = require('express');
const router = express.Router();

const Producto = require('../models/product');

router.get('/', async (req, res) => {
	const products = await Producto.find();
	res.json(products);
});

router.get('/:id', async (req, res) => {
	const producto = await Producto.findById(req.params.id);
	res.json(producto);
});

router.post('/', async (req, res) => {
	const { nombre, cantidad, categoria, precio } = req.body;
	const products = new Producto({ nombre, cantidad, categoria, precio });
	await products.save();
	res.json({status: 'Producto Saved'});
});

router.put('/:id', async (req, res) => {
	const { nombre, cantidad, categoria, precio } = req.body;
	const newProducto = { nombre, cantidad, categoria, precio };
	await Producto.findByIdAndUpdate(req.params.id, newProducto);
	res.json({status: 'Producto Updated!'});
});

router.delete('/:id', async (req, res) => {
	await Producto.findByIdAndRemove(req.params.id);
	res.json({status: 'Product Deleted'});
});

module.exports = router;