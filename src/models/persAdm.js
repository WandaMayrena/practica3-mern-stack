const mongoose = require('mongoose');
const { Schema } = mongoose;

const PersAdmSchema = new Schema({
	nombre: { type: String, required: true },
	cedula: { type: String, required: true},
	edad: { type: Number, required: true},
	genero: { type: String, required: true },
	fecha_nac: { type: Date, required: true},
	direccion: { type: String, required: true},
	usuario: { type: String, required: true },
	contraseña: { type: String, required: true},
	email: { type: String, required: true}
}); 


module.exports = mongoose.model('Personal_adm', PersAdmSchema);