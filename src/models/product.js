const mongoose = require('mongoose');
const { Schema } = mongoose;


const ProductoSchema = new Schema({
	nombre: { type: String, required: true },
	cantidad: { type: Number, required: true},
	categoria: { type: String, enum: ['embutidos', 'vegetales', 'frutas', 'carnes'] },
	precio: { type: Number, required: true }
}); 

module.exports = mongoose.model('Producto', ProductoSchema);